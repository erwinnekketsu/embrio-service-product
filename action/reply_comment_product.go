package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
)

type ReplyCommentProduct struct {
	builder     *builder.Grpc
	repoProduct repo.IProduct
}

func NewReplyCommentProduct() *ReplyCommentProduct {
	return &ReplyCommentProduct{
		builder:     builder.NewGrpc(),
		repoProduct: repo.NewProduct(),
	}
}

func (h *ReplyCommentProduct) Handler(ctx context.Context, req *pb.ReplyCommentProductRequest) error {
	dataProduct := h.builder.ReplyCommentProductRequest(req)
	err := h.repoProduct.ReplyCommentProduct(ctx, dataProduct)
	if err != nil {
		return err
	}
	return nil
}
