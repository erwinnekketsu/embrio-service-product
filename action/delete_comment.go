package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
)

type DeleteComment struct {
	builder     *builder.Grpc
	repoProduct repo.IProduct
}

func NewDeleteComment() *DeleteComment {
	return &DeleteComment{
		builder:     builder.NewGrpc(),
		repoProduct: repo.NewProduct(),
	}
}

func (h *DeleteComment) Handler(ctx context.Context, req *pb.DeleteCommentRequest) error {
	err := h.repoProduct.DeleteComment(ctx, req.Id, req.Tag)
	if err != nil {
		return err
	}
	return nil
}
