package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/util"
)

type ListReplyProduct struct {
	builder     *builder.Grpc
	repoProduct repo.IProduct
}

func NewListReplyProduct() *ListReplyProduct {
	return &ListReplyProduct{
		builder:     builder.NewGrpc(),
		repoProduct: repo.NewProduct(),
	}
}

func (h *ListReplyProduct) Handler(ctx context.Context, req *pb.ListReplyProductRequest) (res []entity.ReplyCommentProduct, pagintaion *util.Pagination, err error) {
	return h.repoProduct.GetReplyCommentProduct(ctx, int(req.CommentId), int(req.Page), int(req.Limit))
}
