package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/util"
)

type ListCommentProduct struct {
	builder     *builder.Grpc
	repoProduct repo.IProduct
}

func NewListCommentProduct() *ListCommentProduct {
	return &ListCommentProduct{
		builder:     builder.NewGrpc(),
		repoProduct: repo.NewProduct(),
	}
}

func (h *ListCommentProduct) Handler(ctx context.Context, req *pb.ListCommentProductRequest) (res []entity.CommentProduct, pagintaion *util.Pagination, err error) {
	return h.repoProduct.GetCommentProduct(ctx, int(req.ProductId), int(req.Page), int(req.Limit))
}
