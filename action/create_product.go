package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
)

type CreateProduct struct {
	builder     *builder.Grpc
	repoProduct repo.IProduct
}

func NewCreateProduct() *CreateProduct {
	return &CreateProduct{
		builder:     builder.NewGrpc(),
		repoProduct: repo.NewProduct(),
	}
}

func (h *CreateProduct) Handler(ctx context.Context, req *pb.CreateProductRequest) error {
	dataProduct := h.builder.CreateProductRequest(req)
	err := h.repoProduct.CreateUpdateProduct(ctx, dataProduct)
	if err != nil {
		return err
	}
	return nil
}
