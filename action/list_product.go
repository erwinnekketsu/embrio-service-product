package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/util"
)

type ListProduct struct {
	builder     *builder.Grpc
	repoProduct repo.IProduct
}

func NewListProduct() *ListProduct {
	return &ListProduct{
		builder:     builder.NewGrpc(),
		repoProduct: repo.NewProduct(),
	}
}

func (h *ListProduct) Handler(ctx context.Context, req *pb.ListProductRequest) (res []entity.Product, pagintaion *util.Pagination, err error) {
	return h.repoProduct.GetProduct(ctx, int(req.Id), int(req.Page), int(req.Limit), req.Query)
}
