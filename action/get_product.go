package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
)

type GetProduct struct {
	builder     *builder.Grpc
	repoProduct repo.IProduct
}

func NewGetProduct() *GetProduct {
	return &GetProduct{
		builder:     builder.NewGrpc(),
		repoProduct: repo.NewProduct(),
	}
}

func (h *GetProduct) Handler(ctx context.Context, req *pb.GetProductRequest) (res entity.Product, err error) {
	return h.repoProduct.GetProductById(ctx, int(req.Id))
}
