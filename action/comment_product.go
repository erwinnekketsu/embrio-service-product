package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
)

type CommentProduct struct {
	builder     *builder.Grpc
	repoProduct repo.IProduct
}

func NewCommentProduct() *CommentProduct {
	return &CommentProduct{
		builder:     builder.NewGrpc(),
		repoProduct: repo.NewProduct(),
	}
}

func (h *CommentProduct) Handler(ctx context.Context, req *pb.CommentProductRequest) error {
	dataProduct := h.builder.CommentProductRequest(req)
	err := h.repoProduct.CommentProduct(ctx, dataProduct)
	if err != nil {
		return err
	}
	return nil
}
