package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
)

type UpdateProduct struct {
	builder     *builder.Grpc
	repoProduct repo.IProduct
}

func NewUpdateProduct() *UpdateProduct {
	return &UpdateProduct{
		builder:     builder.NewGrpc(),
		repoProduct: repo.NewProduct(),
	}
}

func (h *UpdateProduct) Handler(ctx context.Context, req *pb.UpdateProductRequest) error {
	dataProduct := h.builder.UpdateProductRequest(req)
	err := h.repoProduct.CreateUpdateProduct(ctx, dataProduct)
	if err != nil {
		return err
	}
	return nil
}
