package grpc

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/action"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
)

type GrpcServer struct {
	builder *builder.Grpc
}

func (gs *GrpcServer) CreateProduct(ctx context.Context, req *pb.CreateProductRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewCreateProduct().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) UpdateProduct(ctx context.Context, req *pb.UpdateProductRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateProduct().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) ListProduct(ctx context.Context, req *pb.ListProductRequest) (*pb.ListProductResponse, error) {
	var resp pb.ListProductResponse
	data, pagination, err := action.NewListProduct().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resPagination := &pb.Pagination{
		PageSize:     int64(pagination.PageSize),
		CurrentPage:  int64(pagination.CurrentPage),
		TotalPage:    int64(pagination.TotalPage),
		TotalResults: int64(pagination.TotalResult),
	}
	return &pb.ListProductResponse{
		Products:   gs.builder.ListProductResponse(data),
		Pagination: resPagination,
	}, nil
}

func (gs *GrpcServer) GetProduct(ctx context.Context, req *pb.GetProductRequest) (*pb.GetProductResponse, error) {
	var resp pb.GetProductResponse
	data, err := action.NewGetProduct().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resp.Products = gs.builder.GetProductResponse(data)
	return &resp, nil
}

func (gs *GrpcServer) CommentProduct(ctx context.Context, req *pb.CommentProductRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewCommentProduct().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) ReplyCommentProduct(ctx context.Context, req *pb.ReplyCommentProductRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewReplyCommentProduct().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) ListCommentProduct(ctx context.Context, req *pb.ListCommentProductRequest) (*pb.ListCommentProductResponse, error) {
	var resp pb.ListCommentProductResponse
	data, pagination, err := action.NewListCommentProduct().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resPagination := &pb.Pagination{
		PageSize:     int64(pagination.PageSize),
		CurrentPage:  int64(pagination.CurrentPage),
		TotalPage:    int64(pagination.TotalPage),
		TotalResults: int64(pagination.TotalResult),
	}
	return &pb.ListCommentProductResponse{
		Comments:   gs.builder.ListCommentProductResponse(data),
		Pagination: resPagination,
	}, nil
}

func (gs *GrpcServer) ListReplyProduct(ctx context.Context, req *pb.ListReplyProductRequest) (*pb.ListCommentProductResponse, error) {
	var resp pb.ListCommentProductResponse
	data, pagination, err := action.NewListReplyProduct().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resPagination := &pb.Pagination{
		PageSize:     int64(pagination.PageSize),
		CurrentPage:  int64(pagination.CurrentPage),
		TotalPage:    int64(pagination.TotalPage),
		TotalResults: int64(pagination.TotalResult),
	}
	return &pb.ListCommentProductResponse{
		Comments:   gs.builder.ListReplyCommentProductResponse(data),
		Pagination: resPagination,
	}, nil
}

func (gs *GrpcServer) DeleteComment(ctx context.Context, req *pb.DeleteCommentRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewDeleteComment().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func NewGrpcServer() *GrpcServer {
	return &GrpcServer{
		builder: builder.NewGrpc(),
	}
}
