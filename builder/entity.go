package builder

import (
	// "context"
	// "strconv"
	// "time"

	"context"
	"log"
	"os"
	"time"

	"github.com/cloudinary/cloudinary-go"
	"github.com/cloudinary/cloudinary-go/api/uploader"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/entity"
	cld "gitlab.com/erwinnekketsu/embrio-service-product.git/repo/cloudinary"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/util"
)

type Entity struct {
	cloudinary *cloudinary.Cloudinary
}

func NewEntity() *Entity {
	return &Entity{
		cloudinary: cld.Cld,
	}
}

func (e *Entity) CreateUpdateProduct(ctx context.Context, req entity.Product) (data mysql.Product) {
	if req.ID != 0 {
		data.ID = req.ID
	}
	randomName, _ := util.GenerateRandomString(5)
	dateNow := time.Now().UTC()
	product := "product_" + randomName

	go func() {
		ctx = context.Background()
		_, err := e.cloudinary.Upload.Upload(ctx, req.Image, uploader.UploadParams{PublicID: product, Folder: "product"})
		if err != nil {
			log.Println("err")
			log.Println(err)
		}
	}()
	ImageURL := os.Getenv("cloudinary_image_url") + "/product/" + product + ".png"
	data.Nama = req.Nama
	data.Description = req.Description
	data.Category = req.Category
	data.Price = req.Price
	data.Unit = req.Unit
	data.Stock = int(req.Stock)
	data.BusinessId = req.BusinessId
	data.Image = ImageURL
	if req.ID != 0 {
		data.UpdatedAt = &dateNow
	} else {
		data.CreatedAt = &dateNow
	}
	return data
}

func (e *Entity) GetProduct(req mysql.Product) (res entity.Product) {
	res.ID = req.ID
	res.Nama = req.Nama
	res.Description = req.Description
	res.Category = req.Category
	res.Price = req.Price
	res.Stock = int32(req.Stock)
	res.Unit = req.Unit
	res.Image = req.Image
	res.BusinessId = req.BusinessId
	return res
}

func (e *Entity) CreateCommentProduct(ctx context.Context, req entity.CommentProduct) (data mysql.CommentProduct) {
	if req.ID != 0 {
		data.ID = req.ID
	}
	dateNow := time.Now().UTC()
	data.UserId = req.UserId
	data.ProductId = req.ProductId
	data.Description = req.Description
	if req.ID != 0 {
		data.UpdatedAt = &dateNow
	} else {
		data.CreatedAt = &dateNow
	}
	return data
}

func (e *Entity) ReplyCommentProduct(ctx context.Context, req entity.ReplyCommentProduct) (data mysql.ReplyCommentProduct) {
	if req.ID != 0 {
		data.ID = req.ID
	}
	dateNow := time.Now().UTC()
	data.CommentId = req.CommentId
	data.UserId = req.UserId
	data.ProductId = req.ProductId
	data.Description = req.Description
	if req.ID != 0 {
		data.UpdatedAt = &dateNow
	} else {
		data.CreatedAt = &dateNow
	}
	return data
}

func (e *Entity) DeleteComment(ctx context.Context, id int64) (data mysql.CommentProduct) {
	data.ID = int32(id)
	return data
}
