package builder

import (
	// "encoding/json"
	// "strconv"

	"encoding/json"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/entity"
	pbNotif "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/notification"
	pb "gitlab.com/erwinnekketsu/embrio-service-product.git/transport/grpc/proto/product"
)

// Grpc struct grpc builder
type Grpc struct{}

// NewGrpc for initiate builder func
func NewGrpc() *Grpc {
	return &Grpc{}
}

func (g *Grpc) CreateProductRequest(req *pb.CreateProductRequest) (data entity.Product) {
	data.Nama = req.Nama
	data.Description = req.Description
	data.Category = req.Category
	data.Price = req.Price
	data.Unit = req.Unit
	data.Stock = req.Stock
	data.BusinessId = req.BusinessId
	data.Image = req.Image
	return data
}

func (g *Grpc) UpdateProductRequest(req *pb.UpdateProductRequest) (data entity.Product) {
	data.ID = req.Id
	data.Nama = req.Nama
	data.Description = req.Description
	data.Category = req.Category
	data.Price = req.Price
	data.Unit = req.Unit
	data.Stock = req.Stock
	data.BusinessId = req.BusinessId
	data.Image = req.Image
	return data
}

// ListProductResponse generate response
func (g *Grpc) ListProductResponse(data []entity.Product) (res []*pb.Products) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetProductResponse generate response
func (g *Grpc) GetProductResponse(data entity.Product) (res *pb.Products) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

func (g *Grpc) CommentProductRequest(req *pb.CommentProductRequest) (data entity.CommentProduct) {
	data.ProductId = req.ProductId
	data.Description = req.Description
	data.UserId = req.UserId
	return data
}

func (g *Grpc) CommentProductNotifRequest(req entity.CommentProduct, ownerId int) (data pbNotif.CreateNotificationRequest) {
	log.Println(req)
	data.ProductId = int32(req.ProductId)
	data.Content = req.Description
	data.From = int32(req.UserId)
	data.To = int32(ownerId)
	data.Title = "comment product"

	return data
}

func (g *Grpc) ReplyCommentProductNotifRequest(req entity.ReplyCommentProduct, ownerId int) (data pbNotif.CreateNotificationRequest) {
	log.Println(req)
	data.ProductId = int32(req.ProductId)
	data.Content = req.Description
	data.From = int32(req.UserId)
	data.To = int32(ownerId)
	data.Title = "comment product"

	return data
}

func (g *Grpc) ReplyCommentProductRequest(req *pb.ReplyCommentProductRequest) (data entity.ReplyCommentProduct) {
	data.CommentId = req.CommentId
	data.ProductId = req.ProductId
	data.Description = req.Description
	data.UserId = req.UserId
	return data
}

// ListCommentProductResponse generate response
func (g *Grpc) ListCommentProductResponse(data []entity.CommentProduct) (res []*pb.Comment) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// ListReplyCommentProductResponse generate response
func (g *Grpc) ListReplyCommentProductResponse(data []entity.ReplyCommentProduct) (res []*pb.Comment) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}
