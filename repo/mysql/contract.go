package mysql

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/util"
)

type IMysql interface {
	Get(context.Context, *sqlx.DB, interface{}, *util.Query, string) error
	CreateOrUpdate(context.Context, *sqlx.DB, interface{}, string) (lastId int64, err error)
	FindWithPagination(ctx context.Context, db *sqlx.DB, query *util.Query, pagination *util.Pagination, queryString string) (data []Product, paginate *util.Pagination, err error)
	Count(ctx context.Context, db *sqlx.DB, query *util.Query, queryString string) (int32, error)
	FindWithPaginationComment(ctx context.Context, db *sqlx.DB, query *util.Query, pagination *util.Pagination, queryString string) (data []CommentProduct, paginate *util.Pagination, err error)
	FindWithPaginationReply(ctx context.Context, db *sqlx.DB, query *util.Query, pagination *util.Pagination, queryString string) (data []ReplyCommentProduct, paginate *util.Pagination, err error)
}
