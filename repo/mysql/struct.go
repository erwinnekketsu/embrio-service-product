package mysql

import "time"

type Product struct {
	ID          int32      `json:"id" db:"id"`
	Nama        string     `json:"nama" db:"nama"`
	Description string     `json:"description" db:"deskripsi"`
	Category    string     `json:"category" db:"kategori"`
	Price       float32    `json:"price" db:"harga"`
	Stock       int        `json:"stock" db:"stok"`
	Unit        string     `json:"unit" db:"satuan"`
	Image       string     `json:"image" db:"foto"`
	BusinessId  int32      `json:"business_id" db:"idToko"`
	CreatedAt   *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt   *time.Time `json:"updatedAt" db:"updatedAt"`
}

type CommentProduct struct {
	ID          int32      `json:"id" db:"id"`
	Name        string     `json:"name" db:"name"`
	UserId      int64      `json:"user_id" db:"idNasabah"`
	ProductId   int64      `json:"product_id" db:"idProduk"`
	Description string     `json:"description" db:"deskripsi"`
	CreatedAt   *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt   *time.Time `json:"updatedAt" db:"updatedAt"`
}

type ReplyCommentProduct struct {
	ID          int32      `json:"id" db:"id"`
	CommentId   int64      `json:"comment_id" db:"idKomentar"`
	Name        string     `json:"name" db:"name"`
	UserId      int64      `json:"user_id" db:"idNasabah"`
	ProductId   int64      `json:"product_id" db:"idProduk"`
	Description string     `json:"description" db:"deskripsi"`
	CreatedAt   *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt   *time.Time `json:"updatedAt" db:"updatedAt"`
}

type User struct {
	UserId int64 `json:"user_id" db:"idNasabah"`
}
