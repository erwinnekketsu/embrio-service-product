package mysql

const (
	QueryCreateProduk = `
		INSERT INTO produk (nama, deskripsi, kategori, harga, stok, satuan, foto, idToko, createdAt) VALUES (:nama, :deskripsi, :kategori, :harga, :stok, :satuan, :foto, :idToko, :createdAt);
	`

	QueryUpdateProduk = `
		UPDATE produk SET nama=:nama, deskripsi=:deskripsi, kategori=:kategori, harga=:harga, stok=:stok, satuan=:satuan, foto=:foto, idToko=:idToko, updatedAt=:updatedAt WHERE id=:id
	`

	QueryGetProduk = `
		SELECT id, nama, deskripsi, kategori, harga, stok, satuan, foto, idToko FROM produk
	`

	QueryCountProduct = `
		SELECT count(1) FROM produk
	`

	QueryCreateCommentProduk = `
		INSERT INTO komentar (idNasabah, idProduk, deskripsi, createdAt) VALUES (:idNasabah, :idProduk, :deskripsi, :createdAt);
	`

	QueryCountComment = `
		SELECT count(1) FROM komentar
	`

	QueryGetComment = `
		SELECT komentar.id as id, nasabah.namaLengkap as name, komentar.deskripsi as deskripsi, komentar.createdAt as createdAt FROM komentar JOIN nasabah ON komentar.idNasabah = nasabah.id 
	`

	QueryDeleteCommentProduk = `
		DELETE FROM komentar WHERE id = :id
	`

	QueryCreateReplyCommentProduk = `
		INSERT INTO balasKomentar (idKomentar, idNasabah, idProduk, deskripsi, createdAt) VALUES (:idKomentar, :idNasabah, :idProduk, :deskripsi, :createdAt);
	`

	QueryCountReplyComment = `
		SELECT count(1) FROM balasKomentar
	`

	QueryGetReplyComment = `
		SELECT balasKomentar.id as id, nasabah.namaLengkap as name, balasKomentar.deskripsi as deskripsi, balasKomentar.createdAt as createdAt FROM balasKomentar JOIN nasabah ON balasKomentar.idNasabah = nasabah.id
	`

	QueryDeleteReplyCommentProduk = `
		DELETE FROM balasKomentar WHERE id = :id
	`

	QueryGetUserByProdukId = `
		SELECT idNasabah FROM produk JOIN tokoNasabah ON produk.idToko = tokoNasabah.id
	`
)
