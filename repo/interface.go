package repo

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-product.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/util"
)

type IProduct interface {
	CreateUpdateProduct(ctx context.Context, req entity.Product) error
	GetProduct(ctx context.Context, id int, page int, limit int, queryProduct string) (data []entity.Product, pagintaion *util.Pagination, err error)
	GetProductById(ctx context.Context, id int) (data entity.Product, err error)
	CommentProduct(ctx context.Context, req entity.CommentProduct) error
	ReplyCommentProduct(ctx context.Context, req entity.ReplyCommentProduct) error
	GetCommentProduct(ctx context.Context, id int, page int, limit int) (data []entity.CommentProduct, pagintaion *util.Pagination, err error)
	GetReplyCommentProduct(ctx context.Context, id int, page int, limit int) (data []entity.ReplyCommentProduct, pagintaion *util.Pagination, err error)
	DeleteComment(ctx context.Context, id int64, tag string) error
}
