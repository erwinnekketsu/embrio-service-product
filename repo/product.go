package repo

import (
	"context"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/entity"
	grpcClient "gitlab.com/erwinnekketsu/embrio-service-product.git/repo/grpc/client"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-product.git/util"
)

type Product struct {
	builder    *builder.Entity
	builderRpc *builder.Grpc
	iMysql     mysql.IMysql
	embrioDB   *sqlx.DB
}

func NewProduct() *Product {
	return &Product{
		builder:    builder.NewEntity(),
		builderRpc: builder.NewGrpc(),
		iMysql:     mysql.NewClient(),
		embrioDB:   mysql.EmbrioDB,
	}
}

func (p *Product) CreateUpdateProduct(ctx context.Context, req entity.Product) error {
	var query string
	if req.ID == 0 {
		query = mysql.QueryCreateProduk
	} else {
		query = mysql.QueryUpdateProduk
	}
	dataProduct := p.builder.CreateUpdateProduct(ctx, req)
	_, err := p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataProduct, query)
	log.Println(err)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (p *Product) GetProduct(ctx context.Context, id int, page int, limit int, queryProduct string) (data []entity.Product, pagintaion *util.Pagination, err error) {
	var query *util.Query
	if queryProduct == "" {
		query = &util.Query{
			Filter: map[string]interface{}{
				"idToko$eq": id,
			},
		}
	} else {
		query = &util.Query{
			Filter: map[string]interface{}{
				"nama$like": "%" + queryProduct + "%",
			},
		}
		limit = 9999
	}

	datas, paginations, err := p.iMysql.FindWithPagination(
		ctx,
		p.embrioDB,
		query,
		util.NewPagination(int32(page), int32(limit)),
		mysql.QueryGetProduk,
	)

	if err != nil {
		log.Println(err)
		return data, &util.Pagination{}, err
	}

	for _, v := range datas {
		dataProduct := entity.Product{
			ID:          v.ID,
			Nama:        v.Nama,
			Description: v.Description,
			Category:    v.Category,
			Price:       v.Price,
			Stock:       int32(v.Stock),
			Unit:        v.Unit,
			Image:       v.Image,
			BusinessId:  v.BusinessId,
		}
		data = append(data, dataProduct)
	}

	return data, paginations, nil
}

func (p *Product) GetProductById(ctx context.Context, id int) (data entity.Product, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"id$eq": id,
		},
	}

	var dataProduct mysql.Product
	err = p.iMysql.Get(
		ctx,
		p.embrioDB,
		&dataProduct,
		query,
		mysql.QueryGetProduk,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	data = p.builder.GetProduct(dataProduct)

	return data, nil
}

func (p *Product) GetUserByProdukId(ctx context.Context, id int) (data mysql.User, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"produk.id$eq": id,
		},
	}
	err = p.iMysql.Get(
		ctx,
		p.embrioDB,
		&data,
		query,
		mysql.QueryGetUserByProdukId,
	)
	log.Println(err)
	if err != nil {
		log.Println(err)
		return data, err
	}

	return data, nil
}

func (p *Product) CommentProduct(ctx context.Context, req entity.CommentProduct) error {
	var query string
	query = mysql.QueryCreateCommentProduk
	userId, err := p.GetUserByProdukId(ctx, int(req.ProductId))
	if err != nil {
		log.Println(err)
		return err
	}

	dataProductNotif := p.builderRpc.CommentProductNotifRequest(req, int(userId.UserId))
	_, err = grpcClient.CreateNotification(ctx, &dataProductNotif)
	if err != nil {
		log.Println(err)
		return err
	}
	dataProduct := p.builder.CreateCommentProduct(ctx, req)
	_, err = p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataProduct, query)

	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (p *Product) ReplyCommentProduct(ctx context.Context, req entity.ReplyCommentProduct) error {
	var query string
	query = mysql.QueryCreateReplyCommentProduk

	userId, err := p.GetUserByProdukId(ctx, int(req.ProductId))
	if err != nil {
		log.Println(err)
		return err
	}

	dataProductNotif := p.builderRpc.ReplyCommentProductNotifRequest(req, int(userId.UserId))
	_, err = grpcClient.CreateNotification(ctx, &dataProductNotif)
	if err != nil {
		log.Println(err)
		return err
	}

	dataProduct := p.builder.ReplyCommentProduct(ctx, req)
	_, err = p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataProduct, query)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (p *Product) GetCommentProduct(ctx context.Context, id int, page int, limit int) (data []entity.CommentProduct, pagintaion *util.Pagination, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"idProduk$eq": id,
		},
	}

	datas, paginations, err := p.iMysql.FindWithPaginationComment(
		ctx,
		p.embrioDB,
		query,
		util.NewPagination(int32(page), int32(limit)),
		mysql.QueryGetComment,
	)

	if err != nil {
		log.Println(err)
		return data, &util.Pagination{}, err
	}

	for _, v := range datas {
		dataProduct := entity.CommentProduct{
			ID:          v.ID,
			Name:        v.Name,
			Description: v.Description,
			UserId:      v.UserId,
			ProductId:   v.ProductId,
			Date:        v.CreatedAt.String(),
		}
		data = append(data, dataProduct)
	}

	return data, paginations, nil
}

func (p *Product) GetReplyCommentProduct(ctx context.Context, id int, page int, limit int) (data []entity.ReplyCommentProduct, pagintaion *util.Pagination, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"idProduk$eq": id,
		},
	}

	datas, paginations, err := p.iMysql.FindWithPaginationReply(
		ctx,
		p.embrioDB,
		query,
		util.NewPagination(int32(page), int32(limit)),
		mysql.QueryGetReplyComment,
	)

	if err != nil {
		log.Println(err)
		return data, &util.Pagination{}, err
	}

	for _, v := range datas {
		dataProduct := entity.ReplyCommentProduct{
			ID:          v.ID,
			Name:        v.Name,
			Description: v.Description,
			UserId:      v.UserId,
			ProductId:   v.ProductId,
			Date:        v.CreatedAt.String(),
		}
		data = append(data, dataProduct)
	}

	return data, paginations, nil
}

func (p *Product) DeleteComment(ctx context.Context, id int64, tag string) error {
	var query string
	if tag == "comment" {
		query = mysql.QueryDeleteCommentProduk
	} else {
		query = mysql.QueryDeleteReplyCommentProduk
	}
	dataProduct := p.builder.DeleteComment(ctx, id)
	_, err := p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataProduct, query)
	log.Println(err)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
